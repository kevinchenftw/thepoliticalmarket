export const pieData = [
	{
		contracts : 597,
		name : "MD"
	},
	{
		contracts : 530,
		name : "TX"
	},
	{
		contracts : 427,
		name : "CA"
	},
	{
		contracts : 356,
		name : "FL"
	},
	{
		contracts : 335,
		name : "VA"
	},
	{
		contracts : 298,
		name : "CO"
	},
	{
		contracts : 198,
		name : "AK"
	},
	{
		contracts : 180,
		name : "OK"
	},
	{
		contracts : 158,
		name : "MA"
	},
	{
		contracts : 141,
		name : "OH"
	},
	{
		contracts : 134,
		name : "AZ"
	},
	{
		contracts : 120,
		name : "GA"
	},
	{
		contracts : 98,
		name : "AL"
	},
	{
		contracts : 89,
		name : "MO"
	},
	{
		contracts : 88,
		name : "UT"
	},
	{
		contracts : 77,
		name : "NC"
	},
	{
		contracts : 71,
		name : "NY"
	},
	{
		contracts : 70,
		name : "NJ"
	},
	{
		contracts : 64,
		name : "MI"
	},
	{
		contracts : 55,
		name : "CT"
	},
	{
		contracts : 48,
		name : "WA"
	},
	{
		contracts : 45,
		name : "PA"
	},
	{
		contracts : 44,
		name : "TN"
	},
	{
		contracts : 41,
		name : "NV"
	},
	{
		contracts : 39,
		name : "NM"
	},
	{
		contracts : 38,
		name : "KS"
	},
	{
		contracts : 37,
		name : "HI"
	},
	{
		contracts : 37,
		name : "NE"
	},
	{
		contracts : 36,
		name : "IL"
	},
	{
		contracts : 34,
		name : "MS"
	},
	{
		contracts : 29,
		name : "IA"
	},
	{
		contracts : 25,
		name : "WI"
	},
	{
		contracts : 24,
		name : "DC"
	},
	{
		contracts : 23,
		name : "MN"
	},
	{
		contracts : 23,
		name : "DE"
	},
	{
		contracts : 20,
		name : "SC"
	},
	{
		contracts : 15,
		name : "AR"
	},
	{
		contracts : 15,
		name : "NH"
	},
	{
		contracts : 14,
		name : "IN"
	},
	{
		contracts : 14,
		name : "KY"
	},
	{
		contracts : 11,
		name : "SD"
	},
	{
		contracts : 10,
		name : "ID"
	},
	{
		contracts : 10,
		name : "MT"
	},
	{
		contracts : 7,
		name : "ND"
	},
	{
		contracts : 7,
		name : "WY"
	},
	{
		contracts : 6,
		name : "OR"
	},
	{
		contracts : 5,
		name : "LA"
	},
	{
		contracts : 5,
		name : "WV"
	},
	{
		contracts : 0,
		name : "ME"
	},
	{
		contracts : 0,
		name : "VT"
	}
]

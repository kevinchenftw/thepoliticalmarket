export const scatterData = [
	{
		Symbol : "AAL",
		Market_Cap : 15480154360,
		Volume : 13354929
	},
	{
		Symbol : "AAPL",
		Market_Cap : 2130000000000,
		Volume : 28163607
	},
	{
		Symbol : "ABBV",
		Market_Cap : 185000000000,
		Volume : 7177147
	},
	{
		Symbol : "ABC",
		Market_Cap : 23121559643,
		Volume : 70528
	},
	{
		Symbol : "ABNB",
		Market_Cap : 119000000000,
		Volume : 1050986
	},
	{
		Symbol : "ABT",
		Market_Cap : 208000000000,
		Volume : 469930
	},
	{
		Symbol : "AC",
		Market_Cap : 807557811,
		Volume : 4236
	},
	{
		Symbol : "ACGL",
		Market_Cap : 15104984022,
		Volume : 243980
	},
	{
		Symbol : "ACGLO",
		Market_Cap : 0,
		Volume : 2583
	},
	{
		Symbol : "ACGLP",
		Market_Cap : 0,
		Volume : 4424
	},
	{
		Symbol : "ACHC",
		Market_Cap : 5247124018,
		Volume : 176551
	},
	{
		Symbol : "ADBE",
		Market_Cap : 215000000000,
		Volume : 498541
	},
	{
		Symbol : "ADP",
		Market_Cap : 80386430626,
		Volume : 528587
	},
	{
		Symbol : "AEE",
		Market_Cap : 19916244804,
		Volume : 342003
	},
	{
		Symbol : "AEP",
		Market_Cap : 41486166669,
		Volume : 613453
	},
	{
		Symbol : "AEPPL",
		Market_Cap : 0,
		Volume : 4049
	},
	{
		Symbol : "AEPPZ",
		Market_Cap : 0,
		Volume : 5316
	},
	{
		Symbol : "AFG",
		Market_Cap : 10068972091,
		Volume : 144204
	},
	{
		Symbol : "AFGB",
		Market_Cap : 0,
		Volume : 1189
	},
	{
		Symbol : "AFGC",
		Market_Cap : 26568357,
		Volume : 1267
	},
	{
		Symbol : "AFGD",
		Market_Cap : 0,
		Volume : 1578
	},
	{
		Symbol : "AFGE",
		Market_Cap : 0,
		Volume : 11
	},
	{
		Symbol : "AFL",
		Market_Cap : 35200571683,
		Volume : 472922
	},
	{
		Symbol : "AFT",
		Market_Cap : 232817262,
		Volume : 29580
	},
	{
		Symbol : "AHT",
		Market_Cap : 238519518,
		Volume : 2612276
	},
	{
		Symbol : "AHT^D",
		Market_Cap : 0,
		Volume : 2010
	},
	{
		Symbol : "AHT^F",
		Market_Cap : 0,
		Volume : 13622
	},
	{
		Symbol : "AHT^G",
		Market_Cap : 0,
		Volume : 246
	},
	{
		Symbol : "AHT^H",
		Market_Cap : 0,
		Volume : 20
	},
	{
		Symbol : "AHT^I",
		Market_Cap : 0,
		Volume : 500
	},
	{
		Symbol : "AIF",
		Market_Cap : 214074817,
		Volume : 9604
	},
	{
		Symbol : "AINV",
		Market_Cap : 965183213,
		Volume : 198047
	},
	{
		Symbol : "AIR",
		Market_Cap : 1530981536,
		Volume : 31407
	},
	{
		Symbol : "ALE",
		Market_Cap : 3673440595,
		Volume : 44362
	},
	{
		Symbol : "ALK",
		Market_Cap : 8725662055,
		Volume : 273400
	},
	{
		Symbol : "ALLY",
		Market_Cap : 17114767064,
		Volume : 641584
	},
	{
		Symbol : "ALSN",
		Market_Cap : 4764377285,
		Volume : 65079
	},
	{
		Symbol : "AMED",
		Market_Cap : 8645737570,
		Volume : 82645
	},
	{
		Symbol : "AMEH",
		Market_Cap : 1130394199,
		Volume : 4515
	},
	{
		Symbol : "AMG",
		Market_Cap : 6266756769,
		Volume : 41872
	},
	{
		Symbol : "AMGN",
		Market_Cap : 140000000000,
		Volume : 566367
	},
	{
		Symbol : "AMZN",
		Market_Cap : 1560000000000,
		Volume : 676736
	},
	{
		Symbol : "APEN",
		Market_Cap : 162658882,
		Volume : 5863
	},
	{
		Symbol : "APO",
		Market_Cap : 11162939036,
		Volume : 316190
	},
	{
		Symbol : "APO^A",
		Market_Cap : 0,
		Volume : 6104
	},
	{
		Symbol : "APO^B",
		Market_Cap : 0,
		Volume : 2069
	},
	{
		Symbol : "APSG",
		Market_Cap : 1042759350,
		Volume : 76817
	},
	{
		Symbol : "APXT",
		Market_Cap : 582399200,
		Volume : 474125
	},
	{
		Symbol : "APXTU",
		Market_Cap : 0,
		Volume : 450
	},
	{
		Symbol : "APXTW",
		Market_Cap : 0,
		Volume : 422477
	},
	{
		Symbol : "AR",
		Market_Cap : 2984788242,
		Volume : 2047313
	},
	{
		Symbol : "ARES",
		Market_Cap : 8162610387,
		Volume : 55634
	},
	{
		Symbol : "ARES^A",
		Market_Cap : 0,
		Volume : 2424
	},
	{
		Symbol : "ARI",
		Market_Cap : 2052981485,
		Volume : 173976
	},
	{
		Symbol : "ARLP",
		Market_Cap : 736460318,
		Volume : 60972
	},
	{
		Symbol : "ARW",
		Market_Cap : 8096688877,
		Volume : 49701
	},
	{
		Symbol : "ATGE",
		Market_Cap : 2019686438,
		Volume : 34105
	},
	{
		Symbol : "AZN",
		Market_Cap : 129000000000,
		Volume : 4435255
	},
	{
		Symbol : "AZO",
		Market_Cap : 28825255690,
		Volume : 36076
	},
	{
		Symbol : "BA",
		Market_Cap : 149000000000,
		Volume : 7922272
	},
	{
		Symbol : "BAC",
		Market_Cap : 323000000000,
		Volume : 13387987
	},
	{
		Symbol : "BAC^A",
		Market_Cap : 0,
		Volume : 1451
	},
	{
		Symbol : "BAC^B",
		Market_Cap : 0,
		Volume : 1124
	},
	{
		Symbol : "BAC^E",
		Market_Cap : 0,
		Volume : 1000
	},
	{
		Symbol : "BAC^K",
		Market_Cap : 0,
		Volume : 10507
	},
	{
		Symbol : "BAC^L",
		Market_Cap : 0,
		Volume : 328
	},
	{
		Symbol : "BAC^M",
		Market_Cap : 0,
		Volume : 14630
	},
	{
		Symbol : "BAC^N",
		Market_Cap : 0,
		Volume : 27083
	},
	{
		Symbol : "BAC^O",
		Market_Cap : 0,
		Volume : 69030
	},
	{
		Symbol : "BAC^P",
		Market_Cap : 0,
		Volume : 146642
	},
	{
		Symbol : "BAM",
		Market_Cap : 66997632987,
		Volume : 891400
	},
	{
		Symbol : "BATRA",
		Market_Cap : 1657453202,
		Volume : 3921
	},
	{
		Symbol : "BATRK",
		Market_Cap : 1610457192,
		Volume : 49735
	},
	{
		Symbol : "BBY",
		Market_Cap : 28626353389,
		Volume : 441342
	},
	{
		Symbol : "BC",
		Market_Cap : 7915525588,
		Volume : 78278
	},
	{
		Symbol : "BC^A",
		Market_Cap : 0,
		Volume : 8617
	},
	{
		Symbol : "BC^B",
		Market_Cap : 0,
		Volume : 10667
	},
	{
		Symbol : "BC^C",
		Market_Cap : 0,
		Volume : 1221
	},
	{
		Symbol : "BCS",
		Market_Cap : 43514658819,
		Volume : 2784511
	},
	{
		Symbol : "BCSF",
		Market_Cap : 988448277,
		Volume : 16525
	},
	{
		Symbol : "BEN",
		Market_Cap : 15035761622,
		Volume : 740437
	},
	{
		Symbol : "BHE",
		Market_Cap : 1140659071,
		Volume : 28392
	},
	{
		Symbol : "BKR",
		Market_Cap : 24230818322,
		Volume : 1761001
	},
	{
		Symbol : "BKT",
		Market_Cap : 385666301,
		Volume : 18716
	},
	{
		Symbol : "BLK",
		Market_Cap : 110000000000,
		Volume : 68175
	},
	{
		Symbol : "BLL",
		Market_Cap : 27408106565,
		Volume : 313423
	},
	{
		Symbol : "BML^G",
		Market_Cap : 0,
		Volume : 1300
	},
	{
		Symbol : "BML^H",
		Market_Cap : 0,
		Volume : 3591
	},
	{
		Symbol : "BML^J",
		Market_Cap : 0,
		Volume : 457
	},
	{
		Symbol : "BML^L",
		Market_Cap : 0,
		Volume : 5400
	},
	{
		Symbol : "BMY",
		Market_Cap : 139000000000,
		Volume : 2135296
	},
	{
		Symbol : "BRK/A",
		Market_Cap : 0,
		Volume : 820
	},
	{
		Symbol : "BRK/B",
		Market_Cap : 0,
		Volume : 1104145
	},
	{
		Symbol : "BWXT",
		Market_Cap : 6045855389,
		Volume : 32746
	},
	{
		Symbol : "BX",
		Market_Cap : 52090380070,
		Volume : 389359
	},
	{
		Symbol : "BXS",
		Market_Cap : 3479190610,
		Volume : 18259
	},
	{
		Symbol : "BXS^A",
		Market_Cap : 0,
		Volume : 2202
	},
	{
		Symbol : "CAF",
		Market_Cap : 487042024,
		Volume : 2255
	},
	{
		Symbol : "CAH",
		Market_Cap : 16985731670,
		Volume : 261245
	},
	{
		Symbol : "CAT",
		Market_Cap : 125000000000,
		Volume : 560766
	},
	{
		Symbol : "CBOE",
		Market_Cap : 11593368000,
		Volume : 72765
	},
	{
		Symbol : "CBRE",
		Market_Cap : 26047374505,
		Volume : 128463
	},
	{
		Symbol : "CCAP",
		Market_Cap : 507716664,
		Volume : 1813
	},
	{
		Symbol : "CCBG",
		Market_Cap : 454013598,
		Volume : 1792
	},
	{
		Symbol : "CCIV",
		Market_Cap : 7640266500,
		Volume : 12903140
	},
	{
		Symbol : "CCL",
		Market_Cap : 32424159109,
		Volume : 9330185
	},
	{
		Symbol : "CCV",
		Market_Cap : 643750000,
		Volume : 47872
	},
	{
		Symbol : "CCX",
		Market_Cap : 865950000,
		Volume : 335117
	},
	{
		Symbol : "CG",
		Market_Cap : 13025469419,
		Volume : 95533
	},
	{
		Symbol : "CHSCL",
		Market_Cap : 0,
		Volume : 7517
	},
	{
		Symbol : "CHSCM",
		Market_Cap : 0,
		Volume : 446
	},
	{
		Symbol : "CHSCN",
		Market_Cap : 0,
		Volume : 659
	},
	{
		Symbol : "CHSCO",
		Market_Cap : 0,
		Volume : 1452
	},
	{
		Symbol : "CHSCP",
		Market_Cap : 0,
		Volume : 1044
	},
	{
		Symbol : "CHTR",
		Market_Cap : 131000000000,
		Volume : 207412
	},
	{
		Symbol : "CI",
		Market_Cap : 84242211300,
		Volume : 512555
	},
	{
		Symbol : "CLF",
		Market_Cap : 8022079773,
		Volume : 3661204
	},
	{
		Symbol : "CLNE",
		Market_Cap : 2872780317,
		Volume : 1671247
	},
	{
		Symbol : "CLX",
		Market_Cap : 23792161375,
		Volume : 125259
	},
	{
		Symbol : "CMCSA",
		Market_Cap : 261000000000,
		Volume : 3847949
	},
	{
		Symbol : "CME",
		Market_Cap : 74422062998,
		Volume : 408008
	},
	{
		Symbol : "CMI",
		Market_Cap : 39341886681,
		Volume : 119976
	},
	{
		Symbol : "CMS",
		Market_Cap : 16836729238,
		Volume : 283202
	},
	{
		Symbol : "CMS^B",
		Market_Cap : 0,
		Volume : 7
	},
	{
		Symbol : "CMSA",
		Market_Cap : 0,
		Volume : 425
	},
	{
		Symbol : "CMSC",
		Market_Cap : 0,
		Volume : 172
	},
	{
		Symbol : "CMSD",
		Market_Cap : 0,
		Volume : 2772
	},
	{
		Symbol : "CNC",
		Market_Cap : 36605466001,
		Volume : 471586
	},
	{
		Symbol : "CNHI",
		Market_Cap : 20634146260,
		Volume : 634248
	},
	{
		Symbol : "CNK",
		Market_Cap : 2893440084,
		Volume : 1475059
	},
	{
		Symbol : "CNS",
		Market_Cap : 3235700450,
		Volume : 18857
	},
	{
		Symbol : "COF",
		Market_Cap : 58832818761,
		Volume : 848591
	},
	{
		Symbol : "COF^G",
		Market_Cap : 0,
		Volume : 166
	},
	{
		Symbol : "COF^H",
		Market_Cap : 0,
		Volume : 9989
	},
	{
		Symbol : "COF^I",
		Market_Cap : 0,
		Volume : 12350
	},
	{
		Symbol : "COF^J",
		Market_Cap : 0,
		Volume : 27190
	},
	{
		Symbol : "COF^K",
		Market_Cap : 0,
		Volume : 702
	},
	{
		Symbol : "COHN",
		Market_Cap : 37863409,
		Volume : 4365
	},
	{
		Symbol : "COKE",
		Market_Cap : 2905843590,
		Volume : 4653
	},
	{
		Symbol : "COLM",
		Market_Cap : 6904155024,
		Volume : 109622
	},
	{
		Symbol : "COP",
		Market_Cap : 75506812494,
		Volume : 2275409
	},
	{
		Symbol : "CPRT",
		Market_Cap : 25610851082,
		Volume : 116793
	},
	{
		Symbol : "CPT",
		Market_Cap : 10655820921,
		Volume : 57458
	},
	{
		Symbol : "CRH",
		Market_Cap : 36398715200,
		Volume : 194090
	},
	{
		Symbol : "CRHC",
		Market_Cap : 1053630000,
		Volume : 75625
	},
	{
		Symbol : "CSCO",
		Market_Cap : 209000000000,
		Volume : 2975922
	},
	{
		Symbol : "CSL",
		Market_Cap : 8403770923,
		Volume : 26060
	},
	{
		Symbol : "CSX",
		Market_Cap : 69692962301,
		Volume : 565246
	},
	{
		Symbol : "CTAS",
		Market_Cap : 36142910868,
		Volume : 52764
	},
	{
		Symbol : "CUB",
		Market_Cap : 2220799881,
		Volume : 57079
	},
	{
		Symbol : "CVS",
		Market_Cap : 95794477344,
		Volume : 2459628
	},
	{
		Symbol : "CVX",
		Market_Cap : 206000000000,
		Volume : 1601811
	},
	{
		Symbol : "CZR",
		Market_Cap : 19309400906,
		Volume : 608406
	},
	{
		Symbol : "D",
		Market_Cap : 59335985511,
		Volume : 573757
	},
	{
		Symbol : "DAL",
		Market_Cap : 31626548717,
		Volume : 4130004
	},
	{
		Symbol : "DCUE",
		Market_Cap : 0,
		Volume : 2999
	},
	{
		Symbol : "DE",
		Market_Cap : 118000000000,
		Volume : 374522
	},
	{
		Symbol : "DELL",
		Market_Cap : 67144982050,
		Volume : 372398
	},
	{
		Symbol : "DFS",
		Market_Cap : 29893448518,
		Volume : 402947
	},
	{
		Symbol : "DHR",
		Market_Cap : 153000000000,
		Volume : 834428
	},
	{
		Symbol : "DHR^A",
		Market_Cap : 0,
		Volume : 123
	},
	{
		Symbol : "DHR^B",
		Market_Cap : 0,
		Volume : 39
	},
	{
		Symbol : "DIS",
		Market_Cap : 349000000000,
		Volume : 2891439
	},
	{
		Symbol : "DISCA",
		Market_Cap : 35034455279,
		Volume : 4481076
	},
	{
		Symbol : "DISCB",
		Market_Cap : 39444829743,
		Volume : 433
	},
	{
		Symbol : "DISCK",
		Market_Cap : 29600679004,
		Volume : 3684858
	},
	{
		Symbol : "DISH",
		Market_Cap : 19915540178,
		Volume : 561141
	},
	{
		Symbol : "DOW",
		Market_Cap : 48312927544,
		Volume : 1195759
	},
	{
		Symbol : "DRUA",
		Market_Cap : 0,
		Volume : 2486
	},
	{
		Symbol : "DTB",
		Market_Cap : 0,
		Volume : 10846
	},
	{
		Symbol : "DTE",
		Market_Cap : 25213832152,
		Volume : 239203
	},
	{
		Symbol : "DTJ",
		Market_Cap : 0,
		Volume : 1820
	},
	{
		Symbol : "DTP",
		Market_Cap : 0,
		Volume : 39088
	},
	{
		Symbol : "DTW",
		Market_Cap : 0,
		Volume : 1730
	},
	{
		Symbol : "DTY",
		Market_Cap : 0,
		Volume : 1574
	},
	{
		Symbol : "DUK",
		Market_Cap : 71401159946,
		Volume : 631126
	},
	{
		Symbol : "DUK^A",
		Market_Cap : 0,
		Volume : 5643
	},
	{
		Symbol : "DUKB",
		Market_Cap : 0,
		Volume : 10518
	},
	{
		Symbol : "DUKH",
		Market_Cap : 0,
		Volume : 1476
	},
	{
		Symbol : "DVA",
		Market_Cap : 11714412000,
		Volume : 63737
	},
	{
		Symbol : "DVN",
		Market_Cap : 15619285500,
		Volume : 2427741
	},
	{
		Symbol : "EAD",
		Market_Cap : 506076646,
		Volume : 16314
	},
	{
		Symbol : "EBAY",
		Market_Cap : 38907888957,
		Volume : 1039447
	},
	{
		Symbol : "EBS",
		Market_Cap : 4859361000,
		Volume : 38007
	},
	{
		Symbol : "ECL",
		Market_Cap : 60019915261,
		Volume : 345994
	},
	{
		Symbol : "ECPG",
		Market_Cap : 1269495545,
		Volume : 38846
	},
	{
		Symbol : "EDD",
		Market_Cap : 393578604,
		Volume : 86254
	},
	{
		Symbol : "EHTH",
		Market_Cap : 1605875822,
		Volume : 90365
	},
	{
		Symbol : "EIX",
		Market_Cap : 22733342488,
		Volume : 206991
	},
	{
		Symbol : "EL",
		Market_Cap : 105000000000,
		Volume : 93799
	},
	{
		Symbol : "EMR",
		Market_Cap : 53486644962,
		Volume : 320536
	},
	{
		Symbol : "EOD",
		Market_Cap : 227414216,
		Volume : 43558
	},
	{
		Symbol : "EPD",
		Market_Cap : 50635134246,
		Volume : 978426
	},
	{
		Symbol : "EPZM",
		Market_Cap : 904361164,
		Volume : 344243
	},
	{
		Symbol : "ERC",
		Market_Cap : 347431287,
		Volume : 8088
	},
	{
		Symbol : "ERH",
		Market_Cap : 119972966,
		Volume : 6535
	},
	{
		Symbol : "ES",
		Market_Cap : 28397248671,
		Volume : 145186
	},
	{
		Symbol : "ET",
		Market_Cap : 21870955893,
		Volume : 3789355
	},
	{
		Symbol : "ETN",
		Market_Cap : 54253068000,
		Volume : 667534
	},
	{
		Symbol : "ETR",
		Market_Cap : 19952771502,
		Volume : 175558
	},
	{
		Symbol : "EW",
		Market_Cap : 50323730786,
		Volume : 223617
	},
	{
		Symbol : "EXAS",
		Market_Cap : 21148481771,
		Volume : 296941
	},
	{
		Symbol : "EXC",
		Market_Cap : 41919063400,
		Volume : 1356069
	},
	{
		Symbol : "F",
		Market_Cap : 49077203035,
		Volume : 35780683
	},
	{
		Symbol : "F^B",
		Market_Cap : 0,
		Volume : 12946
	},
	{
		Symbol : "F^C",
		Market_Cap : 0,
		Volume : 10136
	},
	{
		Symbol : "FAST",
		Market_Cap : 27373218319,
		Volume : 340500
	},
	{
		Symbol : "FB",
		Market_Cap : 797000000000,
		Volume : 4946038
	},
	{
		Symbol : "FCFS",
		Market_Cap : 2827939192,
		Volume : 16582
	},
	{
		Symbol : "FCX",
		Market_Cap : 49696210000,
		Volume : 6294595
	},
	{
		Symbol : "FDX",
		Market_Cap : 69104566011,
		Volume : 410757
	},
	{
		Symbol : "FE",
		Market_Cap : 19354699335,
		Volume : 862048
	},
	{
		Symbol : "FEIM",
		Market_Cap : 109474618,
		Volume : 848
	},
	{
		Symbol : "FMS",
		Market_Cap : 21163260948,
		Volume : 99281
	},
	{
		Symbol : "FOF",
		Market_Cap : 360511939,
		Volume : 10720
	},
	{
		Symbol : "FTS",
		Market_Cap : 19862340000,
		Volume : 50398
	},
	{
		Symbol : "FWONA",
		Market_Cap : 9697069125,
		Volume : 9459
	},
	{
		Symbol : "FWONK",
		Market_Cap : 10983675237,
		Volume : 96451
	},
	{
		Symbol : "GBX",
		Market_Cap : 1565708616,
		Volume : 27094
	},
	{
		Symbol : "GD",
		Market_Cap : 50253764398,
		Volume : 154822
	},
	{
		Symbol : "GE",
		Market_Cap : 118000000000,
		Volume : 27533830
	},
	{
		Symbol : "GER",
		Market_Cap : 165992219,
		Volume : 20077
	},
	{
		Symbol : "GH",
		Market_Cap : 14491599361,
		Volume : 82140
	},
	{
		Symbol : "GILD",
		Market_Cap : 80202058182,
		Volume : 2158259
	},
	{
		Symbol : "GIS",
		Market_Cap : 36343791385,
		Volume : 509128
	},
	{
		Symbol : "GJS",
		Market_Cap : 0,
		Volume : 1412
	},
	{
		Symbol : "GLW",
		Market_Cap : 31568394720,
		Volume : 554128
	},
	{
		Symbol : "GM",
		Market_Cap : 83054214945,
		Volume : 4684138
	},
	{
		Symbol : "GNW",
		Market_Cap : 1752577891,
		Volume : 673229
	},
	{
		Symbol : "GOLD",
		Market_Cap : 36692948463,
		Volume : 3083081
	},
	{
		Symbol : "GOOG",
		Market_Cap : 1400000000000,
		Volume : 323615
	},
	{
		Symbol : "GOOGL",
		Market_Cap : 1390000000000,
		Volume : 246198
	},
	{
		Symbol : "GPN",
		Market_Cap : 63781432134,
		Volume : 347326
	},
	{
		Symbol : "GS",
		Market_Cap : 118000000000,
		Volume : 517436
	},
	{
		Symbol : "GS^A",
		Market_Cap : 0,
		Volume : 11071
	},
	{
		Symbol : "GS^C",
		Market_Cap : 0,
		Volume : 1663
	},
	{
		Symbol : "GS^D",
		Market_Cap : 0,
		Volume : 27073
	},
	{
		Symbol : "GS^J",
		Market_Cap : 0,
		Volume : 14154
	},
	{
		Symbol : "GS^K",
		Market_Cap : 0,
		Volume : 9135
	},
	{
		Symbol : "GS^N",
		Market_Cap : 0,
		Volume : 919
	},
	{
		Symbol : "GSBD",
		Market_Cap : 2038954872,
		Volume : 68523
	},
	{
		Symbol : "GSEE",
		Market_Cap : 0,
		Volume : 101
	},
	{
		Symbol : "GSID",
		Market_Cap : 0,
		Volume : 1
	},
	{
		Symbol : "GSUS",
		Market_Cap : 0,
		Volume : 1787
	},
	{
		Symbol : "GTIP",
		Market_Cap : 28260000,
		Volume : 612
	},
	{
		Symbol : "GTN",
		Market_Cap : 1880621554,
		Volume : 102797
	},
	{
		Symbol : "HAL",
		Market_Cap : 20154191337,
		Volume : 2156257
	},
	{
		Symbol : "HALL",
		Market_Cap : 67644850,
		Volume : 90486
	},
	{
		Symbol : "HBAN",
		Market_Cap : 16276378481,
		Volume : 2183650
	},
	{
		Symbol : "HBANN",
		Market_Cap : 0,
		Volume : 1000
	},
	{
		Symbol : "HBANO",
		Market_Cap : 0,
		Volume : 9303
	},
	{
		Symbol : "HBANP",
		Market_Cap : 0,
		Volume : 13024
	},
	{
		Symbol : "HD",
		Market_Cap : 304000000000,
		Volume : 1827458
	},
	{
		Symbol : "HE",
		Market_Cap : 4454589859,
		Volume : 60613
	},
	{
		Symbol : "HES",
		Market_Cap : 21212770812,
		Volume : 651279
	},
	{
		Symbol : "HGH",
		Market_Cap : 0,
		Volume : 4793
	},
	{
		Symbol : "HIG",
		Market_Cap : 20166774735,
		Volume : 267531
	},
	{
		Symbol : "HIG^G",
		Market_Cap : 0,
		Volume : 1638
	},
	{
		Symbol : "HII",
		Market_Cap : 7808597977,
		Volume : 27296
	},
	{
		Symbol : "HON",
		Market_Cap : 148000000000,
		Volume : 383689
	},
	{
		Symbol : "HRB",
		Market_Cap : 3797356523,
		Volume : 398278
	},
	{
		Symbol : "HSIC",
		Market_Cap : 9505204085,
		Volume : 117266
	},
	{
		Symbol : "HTH",
		Market_Cap : 3079192640,
		Volume : 49923
	},
	{
		Symbol : "HUM",
		Market_Cap : 52404088253,
		Volume : 186863
	},
	{
		Symbol : "HUN",
		Market_Cap : 6170332732,
		Volume : 207241
	},
	{
		Symbol : "ICE",
		Market_Cap : 64349109929,
		Volume : 550825
	},
	{
		Symbol : "IHRT",
		Market_Cap : 2588816610,
		Volume : 361310
	},
	{
		Symbol : "IIF",
		Market_Cap : 265687670,
		Volume : 8428
	},
	{
		Symbol : "IIIN",
		Market_Cap : 710850555,
		Volume : 5238
	},
	{
		Symbol : "IIVI",
		Market_Cap : 7623533432,
		Volume : 396344
	},
	{
		Symbol : "IIVIP",
		Market_Cap : 0,
		Volume : 967
	},
	{
		Symbol : "INFO",
		Market_Cap : 37830903440,
		Volume : 284438
	},
	{
		Symbol : "INTC",
		Market_Cap : 261000000000,
		Volume : 4583891
	},
	{
		Symbol : "INTU",
		Market_Cap : 107000000000,
		Volume : 195682
	},
	{
		Symbol : "IP",
		Market_Cap : 20929555309,
		Volume : 290184
	},
	{
		Symbol : "J",
		Market_Cap : 16239956339,
		Volume : 62664
	},
	{
		Symbol : "JEF",
		Market_Cap : 8279599844,
		Volume : 149612
	},
	{
		Symbol : "JNJ",
		Market_Cap : 423000000000,
		Volume : 1073839
	},
	{
		Symbol : "JPM^J",
		Market_Cap : 0,
		Volume : 52390
	},
	{
		Symbol : "KE",
		Market_Cap : 716267651,
		Volume : 102673
	},
	{
		Symbol : "KEX",
		Market_Cap : 4067225672,
		Volume : 10506
	},
	{
		Symbol : "KEY",
		Market_Cap : 19696012040,
		Volume : 1675988
	},
	{
		Symbol : "KEY^I",
		Market_Cap : 0,
		Volume : 2239
	},
	{
		Symbol : "KEY^J",
		Market_Cap : 0,
		Volume : 733
	},
	{
		Symbol : "KEY^K",
		Market_Cap : 0,
		Volume : 483
	},
	{
		Symbol : "KKR",
		Market_Cap : 27867618039,
		Volume : 153186
	},
	{
		Symbol : "KKR^A",
		Market_Cap : 0,
		Volume : 3557
	},
	{
		Symbol : "KKR^B",
		Market_Cap : 0,
		Volume : 12
	},
	{
		Symbol : "KKR^C",
		Market_Cap : 0,
		Volume : 3123
	},
	{
		Symbol : "KO",
		Market_Cap : 221000000000,
		Volume : 5663210
	},
	{
		Symbol : "KSU",
		Market_Cap : 19953821115,
		Volume : 61923
	},
	{
		Symbol : "KSU^",
		Market_Cap : 0,
		Volume : 400
	},
	{
		Symbol : "KTOS",
		Market_Cap : 3253252580,
		Volume : 134644
	},
	{
		Symbol : "KW",
		Market_Cap : 2907749216,
		Volume : 65313
	},
	{
		Symbol : "LB",
		Market_Cap : 16589175783,
		Volume : 387366
	},
	{
		Symbol : "LHCG",
		Market_Cap : 6158683332,
		Volume : 16306
	},
	{
		Symbol : "LHX",
		Market_Cap : 39284648769,
		Volume : 261329
	},
	{
		Symbol : "LIN",
		Market_Cap : 141000000000,
		Volume : 328729
	},
	{
		Symbol : "LMT",
		Market_Cap : 96983483886,
		Volume : 382036
	},
	{
		Symbol : "LOW",
		Market_Cap : 128000000000,
		Volume : 414096
	},
	{
		Symbol : "LSXMA",
		Market_Cap : 15746368022,
		Volume : 71571
	},
	{
		Symbol : "LSXMK",
		Market_Cap : 15729465523,
		Volume : 201781
	},
	{
		Symbol : "LUMN",
		Market_Cap : 15262647824,
		Volume : 1597923
	},
	{
		Symbol : "LYFT",
		Market_Cap : 21337737493,
		Volume : 1532750
	},
	{
		Symbol : "LYV",
		Market_Cap : 18784758096,
		Volume : 240270
	},
	{
		Symbol : "MAC",
		Market_Cap : 2083177697,
		Volume : 1450424
	},
	{
		Symbol : "MAR",
		Market_Cap : 48924897962,
		Volume : 341527
	},
	{
		Symbol : "MASI",
		Market_Cap : 12649962690,
		Volume : 84156
	},
	{
		Symbol : "MATX",
		Market_Cap : 3200737677,
		Volume : 28191
	},
	{
		Symbol : "MCBS",
		Market_Cap : 415406404,
		Volume : 765
	},
	{
		Symbol : "MCD",
		Market_Cap : 166000000000,
		Volume : 1169416
	},
	{
		Symbol : "MCK",
		Market_Cap : 29668809698,
		Volume : 94103
	},
	{
		Symbol : "MD",
		Market_Cap : 2354019172,
		Volume : 78548
	},
	{
		Symbol : "MDP",
		Market_Cap : 1565720038,
		Volume : 28463
	},
	{
		Symbol : "MEI",
		Market_Cap : 1774316959,
		Volume : 16034
	},
	{
		Symbol : "MER^K",
		Market_Cap : 0,
		Volume : 6639
	},
	{
		Symbol : "MET",
		Market_Cap : 52701349639,
		Volume : 934211
	},
	{
		Symbol : "MET^A",
		Market_Cap : 0,
		Volume : 7066
	},
	{
		Symbol : "MET^E",
		Market_Cap : 0,
		Volume : 2045
	},
	{
		Symbol : "MET^F",
		Market_Cap : 0,
		Volume : 11159
	},
	{
		Symbol : "MGM",
		Market_Cap : 19818876868,
		Volume : 1734359
	},
	{
		Symbol : "MGR",
		Market_Cap : 0,
		Volume : 3637
	},
	{
		Symbol : "MGRB",
		Market_Cap : 0,
		Volume : 39873
	},
	{
		Symbol : "MGY",
		Market_Cap : 2935795086,
		Volume : 261260
	},
	{
		Symbol : "MO",
		Market_Cap : 91651986839,
		Volume : 2652956
	},
	{
		Symbol : "MOH",
		Market_Cap : 13343480000,
		Volume : 27139
	},
	{
		Symbol : "MPC",
		Market_Cap : 35833141807,
		Volume : 799636
	},
	{
		Symbol : "MPWR",
		Market_Cap : 15553111320,
		Volume : 33953
	},
	{
		Symbol : "MRK",
		Market_Cap : 194000000000,
		Volume : 4079353
	},
	{
		Symbol : "MS",
		Market_Cap : 155000000000,
		Volume : 1657326
	},
	{
		Symbol : "MS^A",
		Market_Cap : 0,
		Volume : 32189
	},
	{
		Symbol : "MS^E",
		Market_Cap : 0,
		Volume : 4274
	},
	{
		Symbol : "MS^F",
		Market_Cap : 0,
		Volume : 4015
	},
	{
		Symbol : "MS^I",
		Market_Cap : 0,
		Volume : 5183
	},
	{
		Symbol : "MS^K",
		Market_Cap : 0,
		Volume : 2140
	},
	{
		Symbol : "MS^L",
		Market_Cap : 0,
		Volume : 822
	},
	{
		Symbol : "MSD",
		Market_Cap : 182053410,
		Volume : 6099
	},
	{
		Symbol : "MSFT",
		Market_Cap : 1770000000000,
		Volume : 5505009
	},
	{
		Symbol : "MTH",
		Market_Cap : 3356210003,
		Volume : 36747
	},
	{
		Symbol : "MTN",
		Market_Cap : 12473612428,
		Volume : 31507
	},
	{
		Symbol : "MUR",
		Market_Cap : 2792423003,
		Volume : 478252
	},
	{
		Symbol : "NC",
		Market_Cap : 155907354,
		Volume : 1351
	},
	{
		Symbol : "NEE",
		Market_Cap : 145000000000,
		Volume : 1947489
	},
	{
		Symbol : "NEE^K",
		Market_Cap : 0,
		Volume : 2752
	},
	{
		Symbol : "NEE^N",
		Market_Cap : 0,
		Volume : 7505
	},
	{
		Symbol : "NEE^O",
		Market_Cap : 0,
		Volume : 321
	},
	{
		Symbol : "NEE^P",
		Market_Cap : 0,
		Volume : 2551
	},
	{
		Symbol : "NEE^Q",
		Market_Cap : 0,
		Volume : 52731
	},
	{
		Symbol : "NEP",
		Market_Cap : 5464275264,
		Volume : 63351
	},
	{
		Symbol : "NFG",
		Market_Cap : 4477036833,
		Volume : 51350
	},
	{
		Symbol : "NI",
		Market_Cap : 9298830942,
		Volume : 1910293
	},
	{
		Symbol : "NI^B",
		Market_Cap : 0,
		Volume : 2872
	},
	{
		Symbol : "NKE",
		Market_Cap : 229000000000,
		Volume : 1347009
	},
	{
		Symbol : "NOC",
		Market_Cap : 51364266927,
		Volume : 170103
	},
	{
		Symbol : "NS",
		Market_Cap : 2037187748,
		Volume : 36324
	},
	{
		Symbol : "NS^A",
		Market_Cap : 0,
		Volume : 1636
	},
	{
		Symbol : "NS^B",
		Market_Cap : 0,
		Volume : 9273
	},
	{
		Symbol : "NS^C",
		Market_Cap : 0,
		Volume : 669
	},
	{
		Symbol : "NSC",
		Market_Cap : 64494416537,
		Volume : 117565
	},
	{
		Symbol : "NUE",
		Market_Cap : 20571125119,
		Volume : 260553
	},
	{
		Symbol : "NVO",
		Market_Cap : 164000000000,
		Volume : 127388
	},
	{
		Symbol : "OC",
		Market_Cap : 9353137781,
		Volume : 127681
	},
	{
		Symbol : "ODFL",
		Market_Cap : 26437142055,
		Volume : 48821
	},
	{
		Symbol : "OMC",
		Market_Cap : 16343243784,
		Volume : 272998
	},
	{
		Symbol : "ONB",
		Market_Cap : 3358179143,
		Volume : 64712
	},
	{
		Symbol : "ORCL",
		Market_Cap : 192000000000,
		Volume : 2025847
	},
	{
		Symbol : "OSK",
		Market_Cap : 8011914781,
		Volume : 85625
	},
	{
		Symbol : "OXY",
		Market_Cap : 26148740934,
		Volume : 6228626
	},
	{
		Symbol : "PBI",
		Market_Cap : 1547323627,
		Volume : 587879
	},
	{
		Symbol : "PBI^B",
		Market_Cap : 0,
		Volume : 15994
	},
	{
		Symbol : "PCH",
		Market_Cap : 3479797776,
		Volume : 94483
	},
	{
		Symbol : "PEG",
		Market_Cap : 29772712131,
		Volume : 547455
	},
	{
		Symbol : "PEGA",
		Market_Cap : 9526859013,
		Volume : 61743
	},
	{
		Symbol : "PEP",
		Market_Cap : 185000000000,
		Volume : 1641714
	},
	{
		Symbol : "PFE",
		Market_Cap : 197000000000,
		Volume : 10383322
	},
	{
		Symbol : "PFH",
		Market_Cap : 0,
		Volume : 13170
	},
	{
		Symbol : "PG",
		Market_Cap : 318000000000,
		Volume : 1063281
	},
	{
		Symbol : "PLUG",
		Market_Cap : 21193229303,
		Volume : 53126831
	},
	{
		Symbol : "PNC",
		Market_Cap : 75106749748,
		Volume : 220032
	},
	{
		Symbol : "PNC^P",
		Market_Cap : 0,
		Volume : 13133
	},
	{
		Symbol : "PNW",
		Market_Cap : 9054206682,
		Volume : 77944
	},
	{
		Symbol : "POR",
		Market_Cap : 4271907312,
		Volume : 66980
	},
	{
		Symbol : "PPL",
		Market_Cap : 21758424492,
		Volume : 1372277
	},
	{
		Symbol : "PRS",
		Market_Cap : 932605704,
		Volume : 7776
	},
	{
		Symbol : "PRU",
		Market_Cap : 36543850000,
		Volume : 405549
	},
	{
		Symbol : "PSF",
		Market_Cap : 333460317,
		Volume : 10868
	},
	{
		Symbol : "PSN",
		Market_Cap : 3844666465,
		Volume : 37857
	},
	{
		Symbol : "PSX",
		Market_Cap : 36631880703,
		Volume : 321564
	},
	{
		Symbol : "PSXP",
		Market_Cap : 6779510271,
		Volume : 224599
	},
	{
		Symbol : "PTA",
		Market_Cap : 1371334468,
		Volume : 12410
	},
	{
		Symbol : "QTWO",
		Market_Cap : 6109830374,
		Volume : 38980
	},
	{
		Symbol : "RAD",
		Market_Cap : 1435419255,
		Volume : 858665
	},
	{
		Symbol : "REGN",
		Market_Cap : 52427137414,
		Volume : 288180
	},
	{
		Symbol : "RELL",
		Market_Cap : 88887511,
		Volume : 4100
	},
	{
		Symbol : "RFI",
		Market_Cap : 376958602,
		Volume : 3961
	},
	{
		Symbol : "RNP",
		Market_Cap : 1126746330,
		Volume : 11472
	},
	{
		Symbol : "RPM",
		Market_Cap : 11368459415,
		Volume : 36873
	},
	{
		Symbol : "RQI",
		Market_Cap : 1820633520,
		Volume : 49648
	},
	{
		Symbol : "RTX",
		Market_Cap : 119000000000,
		Volume : 1180513
	},
	{
		Symbol : "SAFM",
		Market_Cap : 3567727049,
		Volume : 12657
	},
	{
		Symbol : "SBNY",
		Market_Cap : 13576429032,
		Volume : 119533
	},
	{
		Symbol : "SBNYP",
		Market_Cap : 0,
		Volume : 4059
	},
	{
		Symbol : "SBS",
		Market_Cap : 4729888293,
		Volume : 194233
	},
	{
		Symbol : "SCCO",
		Market_Cap : 56442079370,
		Volume : 143112
	},
	{
		Symbol : "SCHW",
		Market_Cap : 123000000000,
		Volume : 1190088
	},
	{
		Symbol : "SCHW^C",
		Market_Cap : 0,
		Volume : 6279
	},
	{
		Symbol : "SCHW^D",
		Market_Cap : 0,
		Volume : 3508
	},
	{
		Symbol : "SEM",
		Market_Cap : 4788122450,
		Volume : 81735
	},
	{
		Symbol : "SFNC",
		Market_Cap : 3435008356,
		Volume : 52475
	},
	{
		Symbol : "SGEN",
		Market_Cap : 26585882451,
		Volume : 282075
	},
	{
		Symbol : "SJM",
		Market_Cap : 13409380710,
		Volume : 81689
	},
	{
		Symbol : "SMG",
		Market_Cap : 12953197479,
		Volume : 39015
	},
	{
		Symbol : "SNDR",
		Market_Cap : 4390406835,
		Volume : 29335
	},
	{
		Symbol : "SNE",
		Market_Cap : 130000000000,
		Volume : 155133
	},
	{
		Symbol : "SO",
		Market_Cap : 64532434985,
		Volume : 818450
	},
	{
		Symbol : "SOJB",
		Market_Cap : 0,
		Volume : 9951
	},
	{
		Symbol : "SOJC",
		Market_Cap : 0,
		Volume : 1789
	},
	{
		Symbol : "SOJD",
		Market_Cap : 0,
		Volume : 5120
	},
	{
		Symbol : "SOJE",
		Market_Cap : 0,
		Volume : 35182
	},
	{
		Symbol : "SOLN",
		Market_Cap : 0,
		Volume : 37644
	},
	{
		Symbol : "SPG",
		Market_Cap : 38451090743,
		Volume : 444323
	},
	{
		Symbol : "SPG^J",
		Market_Cap : 0,
		Volume : 50
	},
	{
		Symbol : "SPR",
		Market_Cap : 5303689809,
		Volume : 542740
	},
	{
		Symbol : "STT",
		Market_Cap : 29476178853,
		Volume : 318591
	},
	{
		Symbol : "STT^D",
		Market_Cap : 0,
		Volume : 16671
	},
	{
		Symbol : "STT^G",
		Market_Cap : 0,
		Volume : 2885
	},
	{
		Symbol : "STZ",
		Market_Cap : 45010093247,
		Volume : 143212
	},
	{
		Symbol : "STZ/B",
		Market_Cap : 0,
		Volume : 25
	},
	{
		Symbol : "SY",
		Market_Cap : 1294050012,
		Volume : 159152
	},
	{
		Symbol : "SYX",
		Market_Cap : 1426161069,
		Volume : 4932
	},
	{
		Symbol : "T",
		Market_Cap : 214000000000,
		Volume : 7524992
	},
	{
		Symbol : "T^A",
		Market_Cap : 0,
		Volume : 12906
	},
	{
		Symbol : "T^C",
		Market_Cap : 0,
		Volume : 29144
	},
	{
		Symbol : "TAK",
		Market_Cap : 61037739798,
		Volume : 1012512
	},
	{
		Symbol : "TBB",
		Market_Cap : 0,
		Volume : 7137
	},
	{
		Symbol : "TBC",
		Market_Cap : 0,
		Volume : 16015
	},
	{
		Symbol : "TDG",
		Market_Cap : 33078045163,
		Volume : 24363
	},
	{
		Symbol : "TELL",
		Market_Cap : 987728855,
		Volume : 3046559
	},
	{
		Symbol : "TFC",
		Market_Cap : 78474313266,
		Volume : 1149593
	},
	{
		Symbol : "TFC^H",
		Market_Cap : 0,
		Volume : 3354
	},
	{
		Symbol : "TFC^I",
		Market_Cap : 0,
		Volume : 4129
	},
	{
		Symbol : "TFC^O",
		Market_Cap : 0,
		Volume : 7100
	},
	{
		Symbol : "TFC^R",
		Market_Cap : 0,
		Volume : 11304
	},
	{
		Symbol : "TGT",
		Market_Cap : 89067808233,
		Volume : 428438
	},
	{
		Symbol : "TKR",
		Market_Cap : 6332824429,
		Volume : 113668
	},
	{
		Symbol : "TM",
		Market_Cap : 213000000000,
		Volume : 40618
	},
	{
		Symbol : "TRV",
		Market_Cap : 39353005811,
		Volume : 287062
	},
	{
		Symbol : "TXT",
		Market_Cap : 12255095750,
		Volume : 100144
	},
	{
		Symbol : "UAL",
		Market_Cap : 19089149747,
		Volume : 5767431
	},
	{
		Symbol : "UEIC",
		Market_Cap : 845912263,
		Volume : 2609
	},
	{
		Symbol : "UNP",
		Market_Cap : 141000000000,
		Volume : 549731
	},
	{
		Symbol : "UPS",
		Market_Cap : 139000000000,
		Volume : 646709
	},
	{
		Symbol : "UTF",
		Market_Cap : 2434203128,
		Volume : 25781
	},
	{
		Symbol : "UUUU",
		Market_Cap : 960814521,
		Volume : 1786295
	},
	{
		Symbol : "V",
		Market_Cap : 482000000000,
		Volume : 1225137
	},
	{
		Symbol : "VAR",
		Market_Cap : 16199448225,
		Volume : 184096
	},
	{
		Symbol : "VLO",
		Market_Cap : 31696309084,
		Volume : 770789
	},
	{
		Symbol : "VRTX",
		Market_Cap : 56187767801,
		Volume : 246094
	},
	{
		Symbol : "VZ",
		Market_Cap : 230000000000,
		Volume : 4181968
	},
	{
		Symbol : "WDAY",
		Market_Cap : 61932213000,
		Volume : 240528
	},
	{
		Symbol : "WFC",
		Market_Cap : 162000000000,
		Volume : 8970916
	},
	{
		Symbol : "WFC^A",
		Market_Cap : 0,
		Volume : 44209
	},
	{
		Symbol : "WFC^C",
		Market_Cap : 0,
		Volume : 87639
	},
	{
		Symbol : "WFC^L",
		Market_Cap : 0,
		Volume : 1729
	},
	{
		Symbol : "WFC^N",
		Market_Cap : 0,
		Volume : 16555
	},
	{
		Symbol : "WFC^O",
		Market_Cap : 0,
		Volume : 1051
	},
	{
		Symbol : "WFC^Q",
		Market_Cap : 0,
		Volume : 24351
	},
	{
		Symbol : "WFC^R",
		Market_Cap : 0,
		Volume : 17177
	},
	{
		Symbol : "WFC^X",
		Market_Cap : 0,
		Volume : 13290
	},
	{
		Symbol : "WFC^Y",
		Market_Cap : 0,
		Volume : 3043
	},
	{
		Symbol : "WFC^Z",
		Market_Cap : 0,
		Volume : 42104
	},
	{
		Symbol : "WHR",
		Market_Cap : 13323882411,
		Volume : 38822
	},
	{
		Symbol : "WM",
		Market_Cap : 51112644589,
		Volume : 736520
	},
	{
		Symbol : "WMB",
		Market_Cap : 28220626591,
		Volume : 1942126
	},
	{
		Symbol : "WMT",
		Market_Cap : 375000000000,
		Volume : 1576855
	},
	{
		Symbol : "WY",
		Market_Cap : 26336388960,
		Volume : 1080154
	},
	{
		Symbol : "XOM",
		Market_Cap : 249000000000,
		Volume : 6014243
	},
	{
		Symbol : "ZION",
		Market_Cap : 9246895971,
		Volume : 226118
	},
	{
		Symbol : "ZIONL",
		Market_Cap : 0,
		Volume : 164
	},
	{
		Symbol : "ZIONN",
		Market_Cap : 0,
		Volume : 3972
	},
	{
		Symbol : "ZIONO",
		Market_Cap : 0,
		Volume : 162
	},
	{
		Symbol : "ZIONP",
		Market_Cap : 0,
		Volume : 2
	}
]


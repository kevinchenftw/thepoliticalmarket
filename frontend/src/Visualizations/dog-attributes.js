export const dogattributes = [ 
  {
    "name": "Baby",
    "Female": 5489,
    "Male": 11455
  },
  {
    "name": "Young",
    "Female": 8578,
    "Male": 19313
  },
  {
    "name": "Adult",
    "Female": 16902,
    "Male": 36956
  },
  {
    "name": "Senior",
    "Female": 2709,
    "Male": 5900
  }
]
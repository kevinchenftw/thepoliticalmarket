export const breedclass = [
    {name: "Working", value: 30},
    {name: "Toy", value: 19},
    {name: "Terrier", value: 19},
    {name: "Sporting", value: 26},
    {name: "Non-Sporting", value: 17},
    {name: "Mixed", value: 2},
    {name: "Hound", value: 23},
    {name: "Herding", value: 21},
    {name: "Other", value: 17}
]

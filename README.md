# ThePoliticalMarket

Website: https://www.thepoliticalmarket.tech/ 

Presentation: https://youtu.be/r-Ka9nCn0c4

## Team Members
| Name | EID | GitLab ID |
| ------ | ------ | ----- |
| Kevin Chen | klc4497 | @kevinchenftw |
| Kevin Liang | krl2322 | @kev-liangg |
| Diyuan Dai | dd33653 | @beastblackga | 
| Vaishnav Bipin | vb7839 | @VaishnavBipin |
| Anisha Kollareddy | ak39675 | @anishakollareddy | 

## Work Estimates (Phase Leader Emboldened)
### Phase 1
| Name | Estimated Hours | Actual Hours | 
|----- | ----- | -----|
| Kevin Chen | 15 | 20 |
| **Kevin Liang** | 15 | 21 |
| Diyuan Dai | 15 | 18 | 
| Vaishnav Bipin | 15 | 16 |
| Anisha Kollareddy | 15 | 15 |  

### Phase 2
| Name | Estimated Hours | Actual Hours | 
|----- | ----- | -----|
| Kevin Chen | 25 | 30 |
| Kevin Liang | 25 | 36 |
| Diyuan Dai | 25 | 32 | 
| **Vaishnav Bipin** | 25 | 40 |
| Anisha Kollareddy | 25 | 5 |  

### Phase 3
| Name | Estimated Hours | Actual Hours | 
|----- | ----- | -----|
| **Kevin Chen** | 25 | 30 |
| Kevin Liang | 25 | 35 |
| Diyuan Dai | 25 | 25 | 
| Vaishnav Bipin | 25 | 30 |
| Anisha Kollareddy | 25 | 25 |  

### Phase 4
| Name | Estimated Hours | Actual Hours | 
|----- | ----- | -----|
| Kevin Chen | 10 | 10 |
| Kevin Liang | 10 | 10 |
| Diyuan Dai | 10 | 10 | 
| Vaishnav Bipin | 10 | 10 |
| **Anisha Kollareddy** | 10 | 10 |  

## Pipelines

https://gitlab.com/kevinchenftw/thepoliticalmarket/-/pipelines  

## Postman Documentation

https://documenter.getpostman.com/view/14826278/Tz5jfLrU

## Git SHA

3cee07986f26d500dd7ec0b35cb1486657170d6c

## Comments
